import Swiper from 'swiper'

export default function metricaInit () {
  let swiperMetrica = ''
  const metricaSlider = document.querySelector('.metricaSlider')
  console.log(metricaSlider)
  if (document.contains(metricaSlider)) {
    swiperMetrica = new Swiper('.metricaSlider', {
      slidesPerView: '1',
      spaceBetween: 0,
      loop: true,
      simulateTouch: false
    })
  }

  if (document.contains(metricaSlider)) {
    var slideControls = Array.from(document.querySelectorAll('.metricaSliderControlItem'))
    slideControls.map(function (el, i) {
      el.addEventListener('click', function () {
        slideControls.map((item) => {
          item.classList.remove('metricaSliderControlItemActive')
        })
        swiperMetrica.slideTo(i + 1)
        el.classList.add('metricaSliderControlItemActive')
      })
    })
  }
}
