import Swiper from 'swiper'

// import 'babel-polyfill'
if (!Array.from) {
  Array.from = function (object) {
      'use strict'
      return [].slice.call(object)
    }
}

document.addEventListener('DOMContentLoaded', function () {
  const body = document.querySelector('body')
  const popup = document.querySelector('.popup')
  const popupOpen = document.querySelectorAll('.js-popup')[0]
  const popupClose = document.querySelector('.popupClose')

  popupOpen.addEventListener('click', function () {
    popup.classList.add('popupShow')
    body.classList.add('popupOpen')
  })

  popupClose.addEventListener('click', function () {
    popup.classList.remove('popupShow')
    body.classList.remove('popupOpen')
  })

  let swiperMetrica = ''
  const metricaSlider = document.querySelector('.metricaSlider')
  swiperMetrica = new Swiper('.metricaSlider', {
    effect: 'fade',
    fade: {
      crossFade: false
    },
    speed: 500,
    slidesPerView: '1',
    spaceBetween: 0,
    loop: true,
    simulateTouch: false

  })

  var slideControls = Array.from(document.querySelectorAll('.metricaSliderControlItem'))
  slideControls.map(function (el, i) {
    el.addEventListener('click', function () {
      slideControls.map((item) => {
          item.classList.remove('metricaSliderControlItemActive')
        })
      swiperMetrica.slideTo(i + 1)
      el.classList.add('metricaSliderControlItemActive')
    })
  })

  let swiperCase = ''
  const caseSlider = document.querySelector('.caseSlider')

  swiperCase = new Swiper('.caseSlider', {
      // effect: 'fade',
      // fade: {
      //   crossFade: false
      // },
    speed: 500,
    slidesPerView: '1',
    spaceBetween: 0,
    loop: true,
    simulateTouch: false

  })

  var caseControls = Array.from(document.querySelectorAll('.caseSliderControlItem'))
  caseControls.map(function (el, i) {
    el.addEventListener('click', function () {
      caseControls.map((item) => {
          item.classList.remove('caseSliderControlItemActive')
        })
      swiperCase.slideTo(i + 1)
      el.classList.add('caseSliderControlItemActive')
    })
  })
})
